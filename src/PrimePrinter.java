
public class PrimePrinter {
	public PrimePrinter() {
		
	}
	
	public void printPrimeNumber(int count){
		System.out.println("Write "+ count + "prime numbers");
		for(int i = 0; i < count; i++){
			if(testPrime(i)){
				System.out.println(i);
			}
			
		}
		System.out.println("Finish writing prime numbers");
	}
	
	private boolean testPrime(int x){
		int divisionNum = 2;
		if(x <= 1){
			return false;
		}
		else{
			while(x > divisionNum){
				if(x % divisionNum == 0){
					return false;
				}
				divisionNum++;
			}
			return true;
		}
	}

}
