import java.util.Random;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class RandomWalk {
	DefaultCategoryDataset data = new DefaultCategoryDataset();
	Random r = new Random();
	
	public RandomWalk(){
		
	}
	
	public void drawRandomWalk(int diffusion){
		int walking = 0;
		
		for(int i = 1; i < diffusion; i++){
			walking = walking + r.nextInt(3)-1;
			data.addValue(walking, "A", i+"");
		}
		
		JFreeChart chart = ChartFactory.createLineChart("ランダムウォーク", "歩数", "逸れ具合", data);
		ChartFrame frame = new ChartFrame("ランダムウォーク", chart);
		//frame.pack();
		frame.setSize(1400, 750);
		frame.setVisible(true);
	}

}
