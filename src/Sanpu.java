import java.util.Random;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Sanpu {
	
	Random r = new Random();
	XYSeriesCollection data = new XYSeriesCollection();
	XYSeries series = new XYSeries("正規分布");
	
	public Sanpu(){
		
	}
	
	public void drawSanpuChart(int numberOfData, int diffusion){
		for(int i = 0; i < numberOfData; i++){
			int x = 0;
			int y = 0;
			
			for(int j = 0; j< diffusion; j++){
				x = x + r.nextInt(3)-1;
			}
			for(int j = 0; j< diffusion; j++){
				y = y + r.nextInt(3)-1;
			}
			series.add(x, y);	
		}
		data.addSeries(series);
		
		JFreeChart randomWalk = ChartFactory.createScatterPlot("ランダムウォーク", "X軸", "Y軸", data);
		
		ChartFrame frame = new ChartFrame("ランダムウォーク", randomWalk);
		frame.pack();
		frame.setSize(1400, 750);
		frame.setVisible(true);
	}

}
