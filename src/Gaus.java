import java.util.ArrayList;
import java.util.Random;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class Gaus {
	DefaultCategoryDataset data = new DefaultCategoryDataset();
	Random r = new Random();
	
	public Gaus(){
		
	}
	
	public void drawGausChart(int numberOfData, int diffusion){
		ArrayList<Integer> category = new ArrayList<Integer>(); //０が出た回数をカウントするための数字
		for(int i = 0; i < numberOfData;i++){
			category.add(0);
		}
		for(int i = 0; i < diffusion;i++){
			
			int counter = 0;   //ゼロがでた回数を数える
			
			for(int j = 0; j < numberOfData;j++){  //コインをたくさん投げまくる
				int coin = r.nextInt(2);
				if(coin == 0){ // ゼロがでたらカウンターを増やす
					counter++;
				}
			}
			
			//ゼロが出た回数と一致するものをカウント
			category.set(counter, category.get(counter)+1);
		}
		int rabel = 0;
		for(int count : category){
			data.addValue(count, "A", rabel + "回");
			System.out.println(rabel + ":" + count);
			rabel++;
		}
		
		JFreeChart chart = ChartFactory.createLineChart("正規分布", "回数", "出現回数", data);
		ChartFrame frame = new ChartFrame("正規分布", chart);
		frame.pack();
		frame.setSize(1400, 750);
		frame.setVisible(true);
		
	}

}
